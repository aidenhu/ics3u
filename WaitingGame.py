import random

NUM_ROUNDS = 5

# https://a1stem.com/courses/ics3u/lessons/ics3u-culminating-activity/
def main():
    accBalance = 0
    sumList = 0
    rolls = 0
    roundTotal = 0
    rolledDoubleSix = 0
    pointsList = []
    rollsOfSum = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    print("Welcome to the Waiting Game where you can earn points to trade in for real items!")
    print("The goal of the game is to get as many points as possible over 5 rounds.")
    print("Each round you can roll a six-sided and a nine-sided dice which will add the sum to your current balance, or bank in your current points to keep them more safe. You can roll the dice as many times as you want during a round.")
    print("There are both advantages and disadvantages of rolling the die multiple times.")
    print("Advantages:\n    If you bank in past a certain milestone, your points get multiplied. 100 being the highest milestone.")
    print("    If you roll at least 5 double sixes, your points get multiplied by 20! The catch is, you don't know your rolls.")
    print("Disadvantages:\n    If at least one of your die rolls a 1, your current points for the round are reset.")
    print("    If the sum of your roll is 7, your current round points are cut in half.")
    print("    If both of your die roll a 1, all your points get reset, including the ones you got outside this round. Ouch")
    print("Have fun!")

    for i in range(1, NUM_ROUNDS + 1):
        while True:
            die6 = random.randint(1, 6)
            die9 = random.randint(1, 9)
            resetAccBalance, points, rolledDoubleSix = computeRoundTotal(roundTotal, die6 + die9, die6, die9, rolledDoubleSix)

            if resetAccBalance:
                accBalance = 0
            if points > 0:
                sumList += points

            rolls += 1
            roundTotal += points
            
            # Why not rollsOfSum[die6 + die9] +=1
            for i in range(2, 16):
                if die6 + die9 == i:
                    rollsOfSum[i - 2] += 1
            
            bankIn = input(f"You rolled {die6} and {die9}. Do you wish to bank in your points now ({str(roundTotal)}" + ") [\"b\" to bank in now, or any other input continue rolling]? ")
            if bankIn == "b":
                break

        roundTotal = posCondition(roundTotal, rolledDoubleSix)
        accBalance += roundTotal
        pointsList.append(roundTotal)
        print("You earned", roundTotal, f"points in round {str(i)}" + "!\n")
        roundTotal = 0
        rolledDoubleSix = 0

    prize(accBalance)
    statistics(rollsOfSum, pointsList, rolls)


def computeRoundTotal(roundTotal, points, die6, die9, rolledDoubleSix):
    if die6 == 6 and die9 == 6:
        rolledDoubleSix = rolledDoubleSix + 1
    
    resetAccBalance, points = negCondition(roundTotal, points, die6, die9)
    return (resetAccBalance, points, rolledDoubleSix)


def posCondition(roundTotal, rolledDoubleSix):
    if rolledDoubleSix >= 5:
        roundTotal = roundTotal * 20
    
    if roundTotal >= 100:
        return roundTotal * 4
    if roundTotal >= 75:
        return roundTotal * 3
    if roundTotal >= 50:
        return roundTotal * 2
    return roundTotal

def negCondition(roundTotal, points, die6, die9):
    if die6 == 1 and die9 == 1:
        return (True, roundTotal * -1)
    elif die6 == 1 or die9 == 1:
        return (False, roundTotal * -1)
    elif die6 + die9 == 7:
        return (False, -1 * (roundTotal // 2))
    return (False, points)

def validate(str, min=-1, max=-1):
    return str.isnumeric() and ((min == -1 or int(str) >= min) and (max == -1 or int(str) <= max))
    
def prize(points):
    costList = [150, 300, 500, 800, 1300, 2000]

    print("Here is the point redeeming section!")
    while True:
        print("\nYou have", points, "points.")
        printOptions(points)
        
        whichPrize = 0
        while True:
            whichPrize = input("Which prize do wish (1/2/3/4/5/6)(0 to quit)? ")
            if validate(whichPrize, 0, 6):
                break

        if int(whichPrize) == 0:
            break

        while True:
            amountPrize = input("How many of that prize do you wish? ")
            if validate(amountPrize, 0, -1):
                break
        if costList[int(whichPrize) - 1] * int(amountPrize) > points:
            print("Error, you don't have enough points.")
        else:
            print("You bought", amountPrize, "copies of prize", whichPrize + ".")
            points -= costList[int(whichPrize) - 1] * int(amountPrize)

def printOptions(points):
    print(f"Prize 1  #{str(points // 150)}: Play the game again! Cost: 150")
    print(f"Prize 2  #{str(points // 300)}: Get a 5% discount for the ride (Does not stack)! Cost: 300")
    print(f"Prize 3  #{str(points // 500)}: Get a 10% discount for the ride (Does not stack)! Cost: 500")
    print(f"Prize 4  #{str(points // 800)}: Get a 15% discount for the ride (Does not stack)! Cost: 800")
    print(f"Prize 5  #{str(points // 1300)}: Get a one time 30% off for food or souvenirs! Cost: 1,300")
    print(f"Prize 6  #{str(points // 2000)}: Earn a champion's certificate! Cost: 2,000")

def statistics(rollsOfSum, pointsList, rolls):
    scale = determineScale(rollsOfSum)

    print("\nYour total number of rolls was", rolls)
    print("Your banked points for each round was")

    for i in range(1, NUM_ROUNDS + 1):
        print(f"{str(i)}:", pointsList[(i - 1)])

    print("\nHere is a horizontal bar graph of *s representing the total number of each possible sum that was rolled in your entire game.")
    print("Scale:", scale)

    for i in range(len(rollsOfSum)):
        print("\n", f"{str(i + 2)}: ", end="")
        for _ in range(rollsOfSum[i] // scale):
            print("*",end="")

def determineScale(rollsOfSum):
    largestNum = rollsOfSum[0]
    
    for i in range(1, len(rollsOfSum)):
        if rollsOfSum[i] > largestNum:
            largestNum = rollsOfSum[i]
    
    scale = 1
    for i in range(1, largestNum + 1):
        if largestNum / (120 * i) > 1:
            scale = i + 1
        else:
            break
    return scale

if (__name__ == "__main__"):
    main()