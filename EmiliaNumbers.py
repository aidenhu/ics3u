RANGE_MIN = 1
RANGE_MAX = 1000

# https://a1stem.com/courses/ics3u/lessons/ics3u-culminating-activity/
def main():
    num = numEmilias(RANGE_MIN, RANGE_MAX)
    print ("There are", num, " Emilia numbers between", RANGE_MIN, "and", RANGE_MAX)

def numEmilias(min, max):
    ret = 0
    for i in range(min, max + 1):
        if isEmilia(i):
            ret += 1
    return ret

def isPrime(factorList):
    return len(factorList) <= 2

def isEmilia(num):
    if num % 10 == 0:
        return True

    factorList = factorsOf(num)
    if (isPrime(factorList)):
        return False
    
    return hasCommonElement(addedFactors(factorList), subtractedFactors(factorList))

# Returns an array of the factors of a given number
def factorsOf(num):
    factorList = []
    
    for i in range(1, num + 1):
        if num % i == 0:
            factorList.append(i)
    return factorList

def addedFactors(factorList):
    addedFactors = [False] * (factorList[len(factorList) - 1] + factorList[len(factorList) - 2] + 1)
    
    for i in range(0, len(factorList) - 1):
        for j in range(i + 1, len(factorList)):
            addedFactors[factorList[i] + factorList[j]] = True
    
    ret = []
    for i in range(1, len(addedFactors)):
        if (addedFactors[i]):
            ret.append(i)
    return ret

def subtractedFactors(factorList):
    subtractedFactors = [False] * (factorList[len(factorList) - 1] - factorList[0] + 1)
    
    for i in range(0, len(factorList) - 1):
        for j in range(i + 1, len(factorList)):
            subtractedFactors[factorList[j] - factorList[i]] = True
    
    ret = []
    for i in range(1, len(subtractedFactors)):
        if (subtractedFactors[i]):
            ret.append(i)
    return ret

# Return True if any element in list1 is in list2
def hasCommonElement(list1, list2):
    for i in range(0, len(list1)):
        for j in range(0, len(list2)):
            if list1[i] == list2[j]:
                return True
    return False


if (__name__ == "__main__"):
    main()