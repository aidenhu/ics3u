# https://a1stem.com/courses/ics3u/lessons/ics3u-culminating-activity/

def main():
    phrase = input("Input your ENCODED phrase here: ")
    print("Your DECODED phrase is", (computePhrase(phrase)))

def computePhrase(phrase):
    lastCharType = ""
    charType = ""
    charAmount = 0
    ret = []
    isCapital = False
    if phrase == "":
        return ""
    
    for i in range(0, len(phrase) + 1):
        # Specific changes for the last iteration of the loop to catch out of index errors
        if i == len(phrase):
            if not(isCapital):
                if charAmount == 0:
                    ret.append('z')
                elif lastCharType == " ":
                    ret.append(" ")
                else:
                    ret.append(chr(charAmount + 96))
            else:
                ret.append(chr(charAmount + 64).upper())
        else:
            charType = phrase[i]
        
        # Lowercase
        if (charType != lastCharType and not(isCapital)) and lastCharType != "":
            if charAmount == 0:
                ret.append('z')
            elif lastCharType == " ":
                ret.append(" ")
            else:
                ret.append(chr(charAmount + 96))
            charAmount = 0
        
        # Uppercase
        elif (charType != lastCharType and isCapital) and lastCharType != "":
            if charAmount == 0:
                ret.append('Z')
            else:
                ret.append(chr(charAmount + 64).upper())
            charAmount = 0

        charAmount += 1
        charAmount %= 26

        # Tests for capitals
        isCapital = ord(charType) < 91 and ord(charType) > 64
        
        lastCharType = charType

    return "".join(ret)

if __name__ == "__main__":
    main()