import pytest
import WaitingGame

@pytest.mark.parametrize("initRoundTotal,points,die6,die9,rolledDoubleSix,ret,isResetAccBalance", [
    pytest.param(0, 6, 1, 5, 0, 0, False, id="Round wipe"),
    pytest.param(0, 2, 1, 1, 0, 0, True, id="Complete wipe"),
    pytest.param(26, 7, 2, 5, 0, -13, False, id="Half of round points"),
    pytest.param(0, 9, 2, 7, 0, 9, False, id="Normal"),
    pytest.param(0, 12, 6, 6, 0, 12, False, id="rolledDoubleSix gets increased"),
])
def test_computeRoundTotal(initRoundTotal, points, die6, die9, rolledDoubleSix, ret, isResetAccBalance):
    resetAccBalance, temp, rolledDoubleSix = WaitingGame.computeRoundTotal(initRoundTotal, points, die6, die9, rolledDoubleSix)
    assert(isResetAccBalance == resetAccBalance)
    assert(temp == ret)

@pytest.mark.parametrize("initRoundTotal,rolledDoubleSix,ret", [
    pytest.param(160, 0,  640, id="x4 Bonus"),
    pytest.param(79, 0,  237, id="x3 Bonus"),
    pytest.param(53, 0,  106, id="x2 Bonus"),
    pytest.param(2, 5,  40, id="x20 Bonus, no other bonus"),
    pytest.param(5, 5,  400, id="x2 Bonus, with other bonus (x4)"),
    pytest.param(4, 5,  240, id="x2 Bonus, with other bonus (x3)"),
    pytest.param(3, 5,  120, id="x2 Bonus, with other bonus (x2)"),
])
def test_posCondition(initRoundTotal,rolledDoubleSix,ret):
    assert(WaitingGame.posCondition(initRoundTotal, rolledDoubleSix) == ret)


# Max 120 stars per row
@pytest.mark.parametrize("rollsOfSum,expectedScale", [
    pytest.param([2, 20, 120], 1, id="Less than 120"),
    pytest.param([2, 21, 121], 2, id="More than 120"),
    pytest.param([124, 241, 362], 4, id="More 360"),
    pytest.param([0, 0, 0], 1, id="All zeros"),
])
def test_determineScale(rollsOfSum,expectedScale):
    assert(WaitingGame.determineScale(rollsOfSum) == expectedScale)