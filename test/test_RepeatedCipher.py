import RepeatedCipher
import pytest

@pytest.mark.parametrize("str, expectedPhrase", [
    pytest.param("", "", id="Blank string"),
    pytest.param("     ", " ", id="Spaces"),
    pytest.param("aaaaaa", "f", id="Single character"),
    pytest.param("ABBBBBb", "AEa", id="Has capitals"),
    pytest.param("********bbbbb&&&&&&&&&&&&999999999999zzzzzzzzzzzzzzz", "hello", id="Lowercase and letter only"),
    pytest.param("lllooooooowwwwwwwwww333333yyyyyyyyyyyyy!!!!!!!!", "cgjfmh", id="Lowercase only, with symbols and letters"),
    pytest.param("aaaaaaaaahhhhhhhhhhhhhhhhhhhhhhhhhhh", "ia", id="Over 26 characters"),
    pytest.param("aaaa aa hhhhh", "d b e", id="Has multiple spaces"),
    pytest.param("AAAAAAAAAAAAAAAAAAAAAAAAAhhhhhqqqqqqqqqqqqqqqqqqq  ####################555555555vvvvvvvvvvvvvv.........................    kkkkkkkkkkkiiiiiiiiiwwwwmmmmggggggggggggggg", "Yes tiny kiddo", id="Yes tiny kiddo special request"),
])
def test_computePhrase(str, expectedPhrase):
    assert(RepeatedCipher.computePhrase(str) == expectedPhrase)