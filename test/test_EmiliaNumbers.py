import pytest
import EmiliaNumbers

@pytest.mark.parametrize("num, expectedBoolean", [
    pytest.param(6, True),
    pytest.param(30, True),
    pytest.param(84, True),
    pytest.param(97, False),
])
def test_isEmilia(num, expectedBoolean):
    assert(EmiliaNumbers.isEmilia(num) == expectedBoolean)

@pytest.mark.parametrize("num, expectedFactors", [
    pytest.param(6, [1, 2, 3, 6], id="6"),
    pytest.param(30, [1, 2, 3, 5, 6, 10, 15, 30], id="30"),
    pytest.param(84, [1, 2, 3, 4, 6, 7, 12, 14, 21, 28, 42, 84], id="84"),
    pytest.param(13, [1, 13], id="13, a prime number"),
])
def test_factorsOf(num, expectedFactors):
    assert(EmiliaNumbers.factorsOf(num) == expectedFactors)

@pytest.mark.parametrize("factorList, expectedAddedFactors", [
    pytest.param([1, 2, 3, 6], [3, 4, 5, 7, 8, 9], id="Added factors for 6"),
    pytest.param([1, 2, 3, 5, 6, 10, 15], [3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 15, 16, 17, 18, 20, 21, 25], id="Added factors for 30"),
    pytest.param([1, 13], [14], id="Added factors for 13, a prime number"),
])
def test_addedFactors(factorList, expectedAddedFactors):
    assert(EmiliaNumbers.addedFactors(factorList) == expectedAddedFactors)

@pytest.mark.parametrize("factorList, expectedSubtractedFactors", [
    pytest.param([1, 2, 3, 6], [1, 2, 3, 4, 5], id="Subtracted factors for 6"),
    pytest.param([1, 2, 3, 5, 6, 10, 15], [1, 2, 3, 4, 5, 7, 8, 9, 10, 12, 13, 14], id="Subtracted factors for 30"),
    pytest.param([1, 13], [12], id="Subtracted factors for 13, a prime number"),
])
def test_subtractedFactors(factorList, expectedSubtractedFactors):
    assert(EmiliaNumbers.subtractedFactors(factorList) == expectedSubtractedFactors)


@pytest.mark.parametrize("min, max, expectedNumEmilias", [
    pytest.param(1, 30, 13),
    pytest.param(1, 6, 2),
    pytest.param(1, 5, 1),
    pytest.param(1, 7, 2),
    pytest.param(1, 1000, 473),
])
def test_numEmilias(min, max, expectedNumEmilias):
    assert(EmiliaNumbers.numEmilias(min, max) == expectedNumEmilias)

@pytest.mark.parametrize("list1, list2, expectedBoolean", [
    pytest.param([3, 4, 5, 7, 8, 9], [1, 2, 3, 4, 5], True),
    pytest.param([3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 15, 16, 17, 18, 20, 21, 25], [97, 1], False),
    pytest.param([3], [3], True),
    pytest.param([3], [4], False),
    pytest.param([], [3], False),
    pytest.param([3], [], False),
    pytest.param([], [], False),
])
def test_hasCommonElement(list1, list2, expectedBoolean):
    assert(EmiliaNumbers.hasCommonElement(list1, list2) == expectedBoolean)
